import 'package:flutter/material.dart';
import 'package:chat/core/color.dart';

ThemeData light = ThemeData(
    textTheme: TextTheme(
        titleLarge: TextStyle(
            fontSize: 24,
            color: Colors.black,
            fontWeight: FontWeight.w500,
            fontFamily: 'Roboto'),
        titleSmall: TextStyle(
            color: Color.fromARGB(255, 167, 167, 167),
            fontSize: 14,
            fontWeight: FontWeight.w500)),
    inputDecorationTheme: InputDecorationTheme(
        border: OutlineInputBorder(borderSide: BorderSide(style: BorderStyle.solid),
          borderRadius: BorderRadius.circular(4),
        ),
        hintStyle: TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 14,
            color: Color(0xFFCFCFCF)), ),
    filledButtonTheme: FilledButtonThemeData(
        style: FilledButton.styleFrom(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(4)),
          foregroundColor: Colors.white, backgroundColor: dblue,
          disabledBackgroundColor: Color(0xFFA7A7A7), disabledForegroundColor: Colors.white

        )));
