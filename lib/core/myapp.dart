import 'package:chat/auth/prestation/pages/signup.dart';
import 'package:flutter/material.dart';
import 'package:chat/core/color.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

void main() async{
  await Supabase.initialize( url: 'https://lwtgrvmdvlhofehpmgtb.supabase.co',
    anonKey: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Imx3dGdydm1kdmxob2ZlaHBtZ3RiIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MDcyMzc4NDYsImV4cCI6MjAyMjgxMzg0Nn0.V6mo8ojP3m2HGKgLzNItYp4k-Mfylz5DYLmEIodd1v0',
  );
  runApp(MyApp());



}
final supabase = Supabase.instance.client;

bool isLight = false;





class MyApp extends StatefulWidget {
  MyApp({super.key});

  void ChangeTheme(BuildContext context){
    isLight = !isLight;
    context.findAncestorStateOfType<MyAppState>()?.onChangetheme();
  }

  @override
  State<MyApp> createState() => MyAppState();
}

class MyAppState extends State<MyApp>{

  void onChangetheme(){
    setState(() {

    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: (!isLight)?LightTheme:DarkTheme,
      home: const SignUp(),
    );
  }
}