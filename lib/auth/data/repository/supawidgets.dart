import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:flutter/material.dart';
import 'package:chat/main.dart';
import 'package:chat/auth/prestation/pages/signup.dart';

Future<User?> sign(email, pass, name, phone) async{
  var res = await supabase.auth.signUp(password: pass, email: email);
  User? user = res.user;
  if (user != null) {
    await supabase.from('profiles').insert({
      'id_user': user.id,
      'phone': phone,
      'fullname': name,
      'avatar': '',
      'balance' : 1000
    });
    return user!;

  }
  else{return null;}

}

Future<User?> login(email, pass,) async{
  var res = await supabase.auth.signInWithPassword(password: pass, email: email);
  User? user = res.user;
  if (user != null) {
    return user!;
  }
  else{return null;}

}

Future<User?> Verifyotp(String otp, String email) async                            {
  var res = await supabase.auth
      .verifyOTP(token: otp, type: OtpType.email, email: email);
  var user = res.user;
  return user;
}

Future<void> newpassword(newpass) async{
  await supabase.auth.updateUser(UserAttributes(password: newpass));
}

Future<Map<String, dynamic>> getUser() async {
  return await supabase
      .from("profiles")
      .select()
      .eq("id_user", supabase.auth.currentUser!.id)
      .single();
}


Future<List<Map<String, dynamic>>> getRiders(rider){
  var res = supabase.from('profiles').select().eq('rider', rider);
  return res;
}

Future<void> createChat(userr, isrider)async{

  if(!isrider){
    await supabase.from('chats').insert({'first_user_id':user!.id, 'second_user_id': userr['id_user']});
  }else{
    await supabase.from('chats').insert({'second_user_id':user!.id, 'first_user_id': userr['id_user']});
  }

}

